# another config-management poc

This one tackles the other 2 use-cases; dependency-injection level configuration override and provider-level etcd integration.

- program.cs shows 
   - standard loading a config file from ConfigurationManager
   - using a custom _materializer_ to populate the section from etcd
   - there are 3 implementations included 
      - a standard read-every-time selector
      - time-based cache selector
      - watch-based cache selector

