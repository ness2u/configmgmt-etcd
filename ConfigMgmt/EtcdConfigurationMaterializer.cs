using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ConfigMgmt
{
  public class EtcdConfigurationMaterializer
  {
    private readonly IValSelector _selector;

    public EtcdConfigurationMaterializer(IValSelector selector)
    {
      _selector = selector;
    }

    public void PopulateElement(object config, string root = "")
    {
      var type = config.GetType();
      foreach (var cmd in GenElementPlan(type, root))
      {
        SetValueCmd setValueCmd;
        CreateCollectionCmd createCollectionCmd;
        CreateAssignInstanceCmd createAssignInstanceCmd;

        if (null != (createAssignInstanceCmd = cmd as CreateAssignInstanceCmd))
        {
          if (_selector.Test(createAssignInstanceCmd.Path))
          {
            var item = createAssignInstanceCmd.CreateAssignInstance(config);
            PopulateElement(item, createAssignInstanceCmd.Path);
          }
        }
        else if (null != (createCollectionCmd = cmd as CreateCollectionCmd))
        {
          var collection = createCollectionCmd.CreateCollection(config);

          foreach (var key in _selector.List(createCollectionCmd.Path))
          {
            var item = createCollectionCmd.CreateAddItem(collection);
            PopulateElement(item, key);
          }                        
        }
        else if (null != (setValueCmd = cmd as SetValueCmd))
        {
          var value = _selector.GetValue(setValueCmd.Path);
          if (value != null)
          {
            setValueCmd.SetValue(config, value);
          }
        }
      }
    }

    private IEnumerable<Cmd> GenElementPlan(Type type, string root = "")
    {
      root = root.TrimEnd('/', ' ');

      var typeName = type.FullName;

      var properties = type.GetProperties();
      foreach (var property in properties.Where(p=>p.CanWrite))
      {
        var propertyName = property.Name;
        var propertyType = property.PropertyType;
        var attributes = property.GetCustomAttributes(typeof(ConfigurationPropertyAttribute), true);
        foreach (var attribute in attributes.Cast<ConfigurationPropertyAttribute>())
        {                    
          var path = string.Format("{0}/{1}", root, attribute.Name);

          if (propertyType.IsValueType || property.PropertyType == typeof(string))
          {
            Console.WriteLine("set-value: {2} ==> {0}", path, typeName, propertyName);
            yield return GenSetValueCmd(path, property);

          }
          else if (propertyType.IsSubclassOf(typeof(ConfigurationElementCollection)))
          {
            Console.WriteLine("create-collection. foreach in: " + path);
            yield return GenCreateCollectionCmd(path, property);
          }
          else
          {
            Console.WriteLine("create-assign-instance: {0} = new {1}", propertyName, propertyType);
            yield return GenCreateAssignInstanceCmd(path, property);
          }
        }
      }
    }

    private CreateAssignInstanceCmd GenCreateAssignInstanceCmd(string path, System.Reflection.PropertyInfo property)
    {
      var cmd = new CreateAssignInstanceCmd()
                {
                  Path = path,
                  CreateAssignInstance = cfg =>
                                         {
                                           var obj = Activator.CreateInstance(property.PropertyType);
                                           property.SetValue(cfg, obj);
                                           return obj;
                                         }
                };
      return cmd;
    }

    private CreateCollectionCmd GenCreateCollectionCmd(string path, System.Reflection.PropertyInfo property)
    {
      var cmd = new CreateCollectionCmd()
                {
                  Path = path,
                  CreateCollection = cfg =>
                                     {
                                       var obj = Activator.CreateInstance(property.PropertyType);
                                       property.SetValue(cfg, obj);
                                       return obj;
                                     },
                  CreateAddItem = GenCollectionAdd(property.PropertyType)
                };
      return cmd;
    }

    private SetValueCmd GenSetValueCmd(string path, System.Reflection.PropertyInfo property)
    {
      var cmd = new SetValueCmd()
                {
                  Path = path,
                  SetValue = (cfg, val) => property.SetValue(cfg, 
                    Convert.ChangeType(val, property.PropertyType))
                };
      return cmd;
    }


    private static Dictionary<Type, Func<object, object>> CollectionAddCache = new Dictionary<Type, Func<object, object>>();

    private Func<object, object> GenCollectionAdd(Type collectionType)
    {            
      Func<object, object> func;
      if (CollectionAddCache.TryGetValue(collectionType, out func))
      {
        return func;
      }

      // BaseAdd(element) vs BaseAdd(element, throw) ???
      var addMethod = typeof(ConfigurationElementCollection)
        .GetMethods(System.Reflection.BindingFlags.NonPublic
                    | System.Reflection.BindingFlags.Instance
                    | System.Reflection.BindingFlags.DeclaredOnly)
        .Where(p => string.Equals(p.Name, "BaseAdd", StringComparison.OrdinalIgnoreCase))
        .First(p => p.GetParameters().Length == 2);

      var itemType = CollectionItemTypeHack(collectionType);

      if (!itemType.IsSubclassOf(typeof(ConfigurationElement)))
      {
        throw new InvalidOperationException();
      }

      func = col =>
             {
               var instance = Activator.CreateInstance(itemType);
               addMethod.Invoke(col, new object[]{ instance, true });
               return instance;
             };
      CollectionAddCache[collectionType] = func;
      return func;
    }

    private Type CollectionItemTypeHack(Type collectionType)
    {            
      var method = collectionType.GetMethod("CreateNewElement", 
        System.Reflection.BindingFlags.NonPublic
        | System.Reflection.BindingFlags.Instance
        | System.Reflection.BindingFlags.DeclaredOnly);

      if (method == null)
      {
        // though, this shouldn't happen
        throw new InvalidOperationException("Cannot determine configuration collection item type.");
      }

      var instance = Activator.CreateInstance(collectionType);
      var newItem = method.Invoke(instance, null);
      return newItem.GetType();
    }

    #region nested classes

    public interface IValSelector
    {
      bool Test(string path);

      IEnumerable<string> List(string path);

      string GetValue(string path);
    }

    class Cmd
    {
      public string Path{ get; set; }
    }

    class SetValueCmd:Cmd
    {
      public Action<object, object> SetValue{ get; set; }
    }

    class CreateAssignInstanceCmd:Cmd
    {
      public Func<object, object> CreateAssignInstance{ get; set; }
    }

    class CreateCollectionCmd:Cmd
    {
      public Func<object, object> CreateCollection{ get; set; }

      public Func<object, object> CreateAddItem{ get; set; }
    }

    #endregion

  }
}