using System.Collections.Generic;
using System.Linq;
using etcetera;

namespace ConfigMgmt
{
  public class EtcdValSelector:EtcdConfigurationMaterializer.IValSelector
  {
    private readonly IEtcdClient _client;

    public EtcdValSelector(IEtcdClient client)
    {
      _client = client;
    }

    #region IValSelector implementation

    public bool Test(string path)
    {
      var response = _client.Get(path, false, false, false);
      var node = response.Node;
      return node != null;
    }

    public IEnumerable<string> List(string path)
    {
      var response = _client.Get(path);
      if (response.Node != null)
      {
        foreach (var key in response.Node.Nodes.Select(n => n.Key))
        {
          yield return key;
        }
      }
    }

    public string GetValue(string path)
    {
      var response = _client.Get(path, false, false, false);
      string value = null;
      if (response.Node != null)
      {
        value = response.Node.Value;
      }
      return value;
    }

    #endregion

  }
}