using System.Collections.Generic;
using etcetera;

namespace ConfigMgmt
{
  public class WatchedValSelector : EtcdConfigurationMaterializer.IValSelector
  {

    private static readonly Dictionary<string, string> ValueCache = new Dictionary<string, string>();
    private static readonly Dictionary<string, HashSet<string>> ListCache = new Dictionary<string, HashSet<string>>();

    private readonly IEtcdClient _client;

    public WatchedValSelector(IEtcdClient client, params string[] paths)
    {
      _client = client;

      foreach (var path in paths)
      {
        var response = _client.Get(path, true);
        UpdateCache(response);
        _client.Watch(path, UpdateCache, true);
      }

    }

    #region IValSelector implementation

    public bool Test(string path)
    {
      var exists = ValueCache.ContainsKey(path) || ListCache.ContainsKey(path);
      return exists;
    }

    public IEnumerable<string> List(string path)
    {
      HashSet<string> set;
      if (ListCache.TryGetValue(path, out set))
      {
        foreach (var key in set)
        {
          yield return key;
        }
      }
    }

    public string GetValue(string path)
    {
      string value = null;
      ValueCache.TryGetValue(path, out value);
      return value;
    }

    #endregion

    private static void UpdateCache(EtcdResponse response)
    {
      if (response.Node != null)
      {
        UpdateCache(response.Node);
      }
    }

    private static void UpdateCache(Node node)
    {
      if (node.Dir)
      {
        var dirset = new HashSet<string>();
        foreach (var n in node.Nodes)
        {
          dirset.Add(n.Key);
          UpdateCache(n);
        }
        ListCache[node.Key] = dirset;
      }
      else
      {
        ValueCache[node.Key] = node.Value;
      }
    }

  }
}