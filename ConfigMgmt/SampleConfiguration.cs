﻿using System;
using System.Configuration;

namespace ConfigMgmt
{
    // the plan was to have SampleConfiguration contain most, if not all,
    // configuration elements observed in regular use.

    public class SampleConfiguration:ConfigurationSection
    {
        [ConfigurationProperty("globalSetting")]
        public string GlobalSetting
        {
            get{ return (string)this["globalSetting"]; }
            set{ this["globalSetting"] = value; }
        }

        [ConfigurationProperty("sample")]
        public SampleElement Sample
        {
            get{ return (SampleElement)this["sample"]; }
            set{ this["sample"] = value; }
        }

        [ConfigurationProperty("sampleCollection")]
        public SampleElementCollection SampleCollection
        {
            get{ return (SampleElementCollection)this["sampleCollection"]; }
            set{ this["sampleCollection"] = value; }
        }

        [ConfigurationProperty("sample2")]
        public SampleElement Sample2
        {
            get{ return (SampleElement)this["sample2"]; }
            set{ this["sample2"] = value; }
        }
    }

    public class SampleElementCollection: ConfigurationElementCollection
    {
        
        protected override ConfigurationElement CreateNewElement()
        {
            return new SampleElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SampleElement)element).Name;
        }

    }

    public class SampleElement : ConfigurationElement
    {
        // should contain
        // - string
        // - int
        // - long
        // - collection

        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("age", IsKey = false)]
        public int Age
        {
            get { return (int)base["age"]; }
            set { base["age"] = value; }
        }

        [ConfigurationProperty("isAlive")]
        public bool IsAlive
        {
            get { return (bool)base["isAlive"]; }
            set { base["isAlive"] = value; }
        }

        [ConfigurationProperty("score")]
        public float Score
        {
            get { return (float)base["score"]; }
            set { base["score"] = value; }
        }

        [ConfigurationProperty("child")]
        public SampleChildElement Child
        {
            get { return (SampleChildElement)base["child"]; }
            set { base["child"] = value; }
        }

    }

    public class SampleChildElement : ConfigurationElement
    {
       
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

    }
}

