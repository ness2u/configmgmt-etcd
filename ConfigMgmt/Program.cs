﻿using System;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using etcetera;
using Moq;

namespace ConfigMgmt
{
  class MainClass
  {
    public static void Main(string[] args)
    {
      var config = (SampleConfiguration)ConfigurationManager.GetSection("sampleConfig");

      var client = new EtcdClient(new Uri("http://localhost:4001/v2/keys/"));

      var valueSelector = new EtcdValSelector(client);
      var cachedValueSelector = new CachedValSelector(valueSelector, 60);
      var watchedSelector = new WatchedValSelector(client,
                                "/config/api",
                                "/config/transactionhistory");

      var materializer = new EtcdConfigurationMaterializer(watchedSelector);
      materializer.PopulateElement(config, "/config/api");
    }
  }
}
