using System;
using System.Collections.Generic;
using System.Linq;

namespace ConfigMgmt
{
  public class CachedValSelector : EtcdConfigurationMaterializer.IValSelector
  {
    private class CacheItem<T>
    {
      public DateTime Expiration { get; set; }

      public string Key { get; set; }

      public T Value { get; set; }


      public bool Expired { get { return Expiration > DateTime.Now; } }
    }

    private static readonly Dictionary<string, CacheItem<bool>> TestCache = new Dictionary<string, CacheItem<bool>>();
    private static readonly Dictionary<string, CacheItem<List<string>>> ListCache = new Dictionary<string, CacheItem<List<string>>>();
    private static readonly Dictionary<string, CacheItem<string>> ValueCache = new Dictionary<string, CacheItem<string>>();

    private readonly int _cacheSeconds;
    private readonly EtcdConfigurationMaterializer.IValSelector _baseSelector;

    public CachedValSelector(EtcdConfigurationMaterializer.IValSelector baseSelector, int cacheSeconds = 60)
    {
      _baseSelector = baseSelector;
      _cacheSeconds = cacheSeconds;
    }

    #region IValSelector implementation

    public bool Test(string path)
    {
      return Cache(TestCache, path, p => _baseSelector.Test(p));
    }

    public IEnumerable<string> List(string path)
    {
      return Cache(ListCache, path, p => _baseSelector.List(p).ToList());
    }

    public string GetValue(string path)
    {
      return Cache(ValueCache, path, p => _baseSelector.GetValue(p));
    }

    #endregion

    private T Cache<T>(Dictionary<string, CacheItem<T>> cache, string key, Func<string, T> selector)
    {
      CacheItem<T> item;
      if (cache.TryGetValue(key, out item) && !item.Expired)
      {
        return item.Value;
      }

      var value = selector(key);
      item = new CacheItem<T>()
      {
        Expiration = DateTime.Now.AddSeconds(_cacheSeconds),
        Key = key,
        Value = value
      };
      cache[key] = item;
      return value;
    }
  }
}